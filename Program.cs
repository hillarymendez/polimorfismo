using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polimorfismo
{

    class Animal   //Clase Base (padre)
    {
        public void animalSound()
        {
            Console.WriteLine("Los animales hacen un sonido");

        }
    }
    class Pig : Animal  //Clase derivada (hijo)
    {
        public void animalSound()
        {
            Console.WriteLine("El cerdo hace wik wik");

        }
    }

    class Dog : Animal  //Clase derivada (hijo)
    {
        public void animalSound()
        {
            Console.WriteLine("El perro hace wuaf wuaf");

        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Animal myAnimal = new Animal(); //creando el objeto animal
            Animal myPig = new Pig(); //Creando el objeto cerdo
            Animal myDog = new Dog(); //Crenado el objeto perro

            myAnimal.animalSound();
            myPig.animalSound();
            myDog.animalSound();

            Console.WriteLine(myAnimal);
            Console.Read();
        }
    }
}
